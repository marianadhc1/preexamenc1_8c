const express = require("express");
const router = express.Router();
const bodyParse = require("body-parser");

router.get("/boleto", (req, res)=>{
    const valores = {
        numBoleto:req.query.numBoleto,
        destino:req.query.destino,
        nombreCliente:req.query.nombreCliente, 
        years:req.query.years,
        tipoViaje:req.query.tipoViaje,
        precio:req.query.precio
    }
    res.render('preExamen.html', valores);
})

router.post("/boleto", (req, res)=>{
    const valores = {
        numBoleto:req.body.numBoleto,
        destino:req.body.destino,
        nombreCliente:req.body.nombreCliente, 
        years:req.body.years,
        tipoViaje:req.body.tipoViaje,
        precio:req.body.precio
    }
    res.render('preExamen.html', valores);
})

module.exports = router; 