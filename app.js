const http = require("http"); 
const express = require("express");
const bodyparser = require("body-parser"); //(requiriendo)
const misRutas = require("./router/index"); 
const path = require("path");

const app = express();
app.set("view engine", "ejs")
app.use(express.static(__dirname + '/public'));
app.engine('html',require('ejs').renderFile)

app.use(bodyparser.urlencoded({extended:true})); //por donde saldrá la codificación(usando)
app.use(misRutas);

//ERROR-------------------------------------------
app.use((req, res, next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html');
})

//escuchar el servidor por el puerto 500
const puerto = 503;
app.listen(puerto,()=>{
    console.log("Iniciando puerto: " + puerto);
})